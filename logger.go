package log

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

// SimpleLog is like fmt.Print without any fancy formats
type SimpleLog func(...interface{})

// FormatteedLog is like fmt.Printf with fancy formatting
type FormattedLog func(string, ...interface{})

// loggerInstance is the singleton instance of our underlying
// logger implementation
var loggerInstance = logrus.New()

// init sets up the defaults here
func init() {
	loggerInstance.SetLevel(defaultLevel)
	loggerInstance.SetOutput(os.Stderr)
	loggerInstance.SetReportCaller(true)
	loggerInstance.SetFormatter(defaultFormatter)
}

// Print writes the log to stdout
var Print,
	// Trace writes the log at the trace level
	Trace,
	// Debug writes the log at the debug level
	Debug,
	// Info writes the log at the info level
	Info,
	// Warn writes the log at the warn level
	Warn,
	// Error writes the log at the error level
	Error SimpleLog = print, loggerInstance.Trace, loggerInstance.Debug, loggerInstance.Info, loggerInstance.Warn, loggerInstance.Error

// Printf writes the log to stdout with formatting
var Printf,
	// Tracef writes the log at the trace level with formatting
	Tracef,
	// Debugf writes the log at the debug level with formatting
	Debugf,
	// Infof writes the log at the info level with formatting
	Infof,
	// Warnf writes the log at the warn level with formatting
	Warnf,
	// Errorf writes the log at the error level with formatting
	Errorf FormattedLog = printf, loggerInstance.Tracef, loggerInstance.Debugf, loggerInstance.Infof, loggerInstance.Warnf, loggerInstance.Errorf

// print is the underlying SimpleLog implementation for a pure
// stdout print
func print(args ...interface{}) {
	args = append(args, "\n")
	fmt.Print(args...)
}

// printf is the underlying FormattedLog implementation for a
// pure stdout print
func printf(message string, args ...interface{}) {
	fmt.Print(fmt.Sprintf(message, args...) + "\n")
}
