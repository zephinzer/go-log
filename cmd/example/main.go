package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/ottermations/go-log"
)

const (
	LineBreak = "- - - - - - - - - -"
)

func main() {
	logLevel := os.Getenv("LEVEL")
	if level, err := strconv.Atoi(logLevel); err == nil { // if logLevel is ok
		log.SetLevel(log.Level(level))
	}

	format := os.Getenv("FORMAT")
	log.SetFormatter(log.Format(format))
	log.Printf("%s\ndefault outputs to os.Stderr (filter out with appending '2>/dev/null' to invocation)\n%s", LineBreak, LineBreak)
	writeLogs("stderr")

	log.Printf("%s\noutput to os.Stdout (filter out with appending '1>/dev/null' to invocation)\n%s", LineBreak, LineBreak)
	log.SetOutput(os.Stdout)
	writeLogs("stdout")

	var buffer bytes.Buffer
	log.Printf("%s\noutput to bytes.Buffer (printed using fmt.Print to os.Stdout)\n%s", LineBreak, LineBreak)
	writer := bufio.NewWriter(&buffer)
	log.SetOutput(writer)
	writeLogs("custom (stdout)")
	writer.Flush()

	fmt.Print(buffer.String())
}

func writeLogs(id string) {
	log.Print("[" + id + "] message from Print")
	log.Printf("["+id+"] message from %s", "Printf")
	log.Trace("[" + id + "] message from Trace")
	log.Tracef("["+id+"] message from %s", "Tracef")
	log.Debug("[" + id + "] message from Debug")
	log.Debugf("["+id+"] message from %s", "Debugf")
	log.Info("[" + id + "] message from Info")
	log.Infof("["+id+"] message from %s", "Infof")
	log.Warn("[" + id + "] message from Warn")
	log.Warnf("["+id+"] message from %s", "Warnf")
	log.Error("[" + id + "] message from Error")
	log.Errorf("["+id+"] message from %s", "Errorf")
}
