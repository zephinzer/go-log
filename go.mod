module gitlab.com/zephinzer/go-log

go 1.19

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/sirupsen/logrus v1.9.0
	gitlab.com/ottermations/go-log v1.0.0
)

require golang.org/x/sys v0.2.0 // indirect
