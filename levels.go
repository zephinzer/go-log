package log

import "github.com/sirupsen/logrus"

// Level defines an accepted log level by this package
type Level uint

const (
	// LevelTrace defines the 'trace' level of logging
	LevelTrace Level = iota
	// LevelDebug defines the 'debug' level of logging
	LevelDebug
	// LevelInfo defines the 'info' level of logging
	LevelInfo
	// LevelWarn defines the 'warning' level of logging
	LevelWarn
	// LevelError defines the 'error' level of logging
	LevelError
)

// levelMap is a convenience hashmap for use internally
// for mapping our Level interface with the underlying
// logger library's levels
var levelMap = map[Level]logrus.Level{
	LevelTrace: logrus.TraceLevel,
	LevelDebug: logrus.DebugLevel,
	LevelInfo:  logrus.InfoLevel,
	LevelWarn:  logrus.WarnLevel,
	LevelError: logrus.ErrorLevel,
}

// Levels defines the available levels, useful for listing
// the available levels in packages that consume this library
var Levels []Level

func init() {
	// we do it this way so that only mapped levels
	// will appear in the list of available log levels
	for level, _ := range levelMap {
		Levels = append(Levels, level)
	}
}

var defaultLevel = levelMap[LevelTrace]

// SetLevel sets the level of logs to display and
// is an exposed interface that interacts with the
// underlying log library
func SetLevel(l Level) {
	loggerInstance.SetLevel(levelMap[l])
}
