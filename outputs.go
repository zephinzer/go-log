package log

import "io"

// SetOutput sets the output stream
func SetOutput(output io.Writer) {
	loggerInstance.SetOutput(output)
}
