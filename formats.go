package log

import "github.com/sirupsen/logrus"

// Format defines the type of an accepted format
type Format string

const (
	// FormatText defines the 'text' output format
	FormatText Format = "text"

	// FormatJson defines the 'json' output format
	FormatJson Format = "json"
)

// formatMap is an internally used hashmap for mapping our
// exposed format interfaces with the underyling logger
// library's implementation
var formatMap = map[Format]logrus.Formatter{
	FormatJson: &logrus.JSONFormatter{
		CallerPrettyfier: prettifyCaller,
		TimestampFormat:  TimestampYmdHMSz,
	},
	FormatText: &logrus.TextFormatter{
		CallerPrettyfier:       prettifyCaller,
		TimestampFormat:        TimestampYmdHMSz,
		FullTimestamp:          true,
		QuoteEmptyFields:       true,
		DisableSorting:         true,
		DisableLevelTruncation: false,
	},
}

// Formats contains a list of available formats that
// is accepted by us and can be useful for providing a
// list of valid formats in packages that consume this
// library
var Formats []Format

func init() {
	// we do it this way so that only mapped formatters
	// will appear in the list of available log formats
	for format, _ := range formatMap {
		Formats = append(Formats, format)
	}
}

// defaultFormatter is the default formatter used if no
// options are specified
var defaultFormatter = formatMap[FormatText]

// SetFormatter sets the format of the logs
func SetFormatter(f Format) {
	if _, ok := formatMap[f]; ok {
		loggerInstance.SetFormatter(formatMap[f])
		return
	}
	loggerInstance.SetFormatter(defaultFormatter)
}
