package log

import (
	"fmt"
	"os"
	"strconv"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var AlertTarget = defaultAlertTarget

// Alertf is a convenience function to execute an Alert with a
// formatted string
func Alertf(t alertType, message string, formatting ...interface{}) error {
	return Alert(t, fmt.Sprintf(message, formatting...))
}

// Alert executes sending of an alert
func Alert(t alertType, message string) error {
	fullMessage := message
	switch t {
	case AlertTypeError:
		Errorf(message)
	case AlertTypeInfo:
		fallthrough
	default:
		Infof(message)
	}

	switch AlertTarget {
	case AlertTargetTelegram:
		return sendTelegramAlert(t, fullMessage)
	default:
		return fmt.Errorf("failed to receive a valid alertType[%s]", t)
	}

	return nil
}

func sendTelegramAlert(t alertType, message string) error {
	// retrieve chat id
	chatId, err := strconv.ParseInt(os.Getenv(EnvKeyAlertTelegramChatId), 10, 64)
	if err != nil {
		return fmt.Errorf("failed to parse provided envvar[%s][%v] into an integer: %s", EnvKeyAlertTelegramChatId, os.Getenv(EnvKeyAlertTelegramChatId), err)
	}
	if chatId == 0 {
		return fmt.Errorf("failed to find value of envvar[%s]", EnvKeyAlertTelegramChatId)
	}

	// retrieve api token
	token := os.Getenv(EnvKeyAlertTelegramToken)
	if token == "" {
		return fmt.Errorf("failed to find value of envvar[%s]", EnvKeyAlertTelegramToken)
	}

	// init bot
	botInstance, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return fmt.Errorf("failed to instantiate a telegram bot instance: %s", err)
	}

	timestamp := time.Now().UTC().Format(time.RFC3339)

	switch t {
	case AlertTypeError:
		message = "⚠️ #ERROR ⚠️\n```\n" + message + "\n" + "\n```\n" + "⏱ `" + timestamp + "`"
	case AlertTypeInfo:
		fallthrough
	default:
		message = "ℹ️ #INFO ℹ️\n```\n" + message + "\n" + "\n```\n" + "⏱ `" + timestamp + "`"
	}

	// init message
	messageInstance := tgbotapi.NewMessage(chatId, message)
	messageInstance.ParseMode = "markdown"

	// send message
	if _, err := botInstance.Send(messageInstance); err != nil {
		return fmt.Errorf("failed to send message: %s", err)
	}
	return nil
}
